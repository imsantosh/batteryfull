package com.android.santosh.batteryfull;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.MediaPlayer;
import android.os.BatteryManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.io.InputStream;


public class MainActivity extends ActionBarActivity
{

     MediaPlayer m =new MediaPlayer();
    String result_play;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    //     result_play = batteryLevel(getApplicationContext());
    //    Log.d("Battery Level",result_play);
    //    Boolean result_stop = batteryDetached(getApplicationContext());


        Button b1 = (Button)findViewById(R.id.button_Start);
        Button b2 = (Button)findViewById(R.id.button1_Stop);

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playMusic();
            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopPlay();
            }
        });

    //    final MediaPlayer mpButtonClick1 = MediaPlayer.create(this, R.raw.spalshm);



      /*  Boolean b = func(getApplicationContext());

        Toast.makeText(getApplicationContext(), b+"", Toast.LENGTH_LONG).show();*/

       /* if(result_play.equalsIgnoreCase("full"))
        {
            Toast.makeText(getApplicationContext(), "Music !!", Toast.LENGTH_LONG).show();
            playMusic();
        }
        if(result_play.equalsIgnoreCase("detached"))
        {

            Toast.makeText(getApplicationContext(), "Battery detached", Toast.LENGTH_LONG).show();
            stopPlay();
        }*/

      /*  if(result_play.equalsIgnoreCase("77"))
        {
            Toast.makeText(getApplicationContext(),result_play,Toast.LENGTH_LONG).show();
            playMusic();



        }*/

    }

    @Override
    protected void onResume() {
        super.onResume();

      /*  if(result_play.equalsIgnoreCase("full"))
        {
            Toast.makeText(getApplicationContext(), "Music !!", Toast.LENGTH_LONG).show();
            playMusic();
        }
        if(result_play.equalsIgnoreCase("detached"))
        {

            Toast.makeText(getApplicationContext(), "Battery detached", Toast.LENGTH_LONG).show();
            stopPlay();
        }*/

    }

    public void playMusic()
    {

        try {

           /* MyApplication app = (MyApplication) getApplication();
            MediaPlayer m = app.m;*/
            AssetFileDescriptor descriptor = getAssets().openFd("babylaugh.mp3");
            m.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();

            m.prepare();
            m.setVolume(1f, 1f);
            m.setLooping(true);
            m.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public static String batteryLevel(Context context)
    {
        String returnVal="nothing";
        Intent intent  = context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int    level   = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
        int    scale   = intent.getIntExtra(BatteryManager.EXTRA_SCALE, 100);
        int    status  = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);

        boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING;
        int    percent = (level*100)/scale;
     //   return String.valueOf(percent) ;

        if(percent == 79){
            System.out.println("Percent----->"+percent);
            returnVal= "full";
        }
        if(isCharging == false)
        {
            System.out.println("Detached----->");
            returnVal= "detached";
        }

     return returnVal;

}

   /* public static Boolean batteryDetached(Context context)
    {
        Intent intent = context.registerReceiver(null,new IntentFilter(Intent.ACTION_POWER_DISCONNECTED));
        int m = intent.getIntExtra(P)
        return true;
    }*/

    public boolean func(Context context){
        Intent intent  = context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING;
                /*||status == BatteryManager.BATTERY_STATUS_CHARGING;*/
        return isCharging;
    }

    public void stopPlay() {
        if (m.isPlaying())
        {
            m.stop();
            m.release();
            m = new MediaPlayer();
        }
    }

}
